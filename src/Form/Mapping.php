<?php
/**
 * @file
 * Contains Attribute for miniOrange SAML IDP Module.
 */

 /**
 * Showing Settings form.
 */
namespace Drupal\miniorange_saml_idp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\miniorange_saml_idp\Utilities;
use Drupal\miniorange_saml_idp\MiniorangeSAMLIdpConstants;

class Mapping extends FormBase {

  public function getFormId() {
    return 'miniorange_saml_mapping';
  }


 public function buildForm(array $form, FormStateInterface $form_state)
 {
     $base_url = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getBaseUrl();

     $form['markup_library'] = array(
         '#attached' => array(
           'library' => array(
             'miniorange_saml_idp/miniorange_saml_idp.admin',
             'core/drupal.dialog.ajax',
           )
         ),
     );

      $form['markup_idp_attr_header'] = array(
          '#markup' => t('<div class="mo_saml_table_layout_1"><div class="mo_saml_table_layout mo_saml_container">'),
      );

     /**
      * Create container to hold @IdentityProviderMapping form elements.
      */
     $form['mo_saml_idp_mapping'] = array(
         '#type' => 'container',
         '#attributes' => array( 'style' => 'padding:2% 2% 5%; margin-bottom:2%' ),
     );

     $form['mo_saml_idp_mapping']['miniorange_saml_idp_div_st'] = array(
         '#markup' => t('<div class="mo_saml_idp_font_for_heading">Attribute Mapping</div>
                               <p style="clear: both"></p><hr><br>'),
     );
   $form['mo_saml_idp_mapping']['markup_idp_sp_note'] = array(
     '#markup' => t('<div class = "mo_saml_highlight_background_note" ><b>Note:</b></divspan> This attribute will be sent in the NameID (unique identifier) in the SAML Response.</div>
         '),
   );

   $form['mo_saml_idp_mapping']['miniorange_saml_idp_nameid_attr_map'] = array(
     '#type' => 'radios',
     '#title' => t('NameID Attribute:'),
     '#options' => array(
       'emailAddress' => t('Drupal Email Address'),
       'username' => t('Drupal Username'),),
     '#default_value' =>\Drupal::config('miniorange_saml_idp.settings')->get('miniorange_saml_idp_nameid_attr_map'),
   );

   $form['mo_saml_idp_mapping']['minioranghe_saml_save_signin'] = array(
     '#type' => 'submit',
     '#button_type' => 'primary',
     '#value' => t('Save Configuration'),
   );

     $form['miniorange_saml_idp_additional_user_attrs'] = array(
       '#type' => 'details',
       '#title' => t('Additional User Attributes <a href="' . $base_url . MiniorangeSAMLIdpConstants::LICENSE_PAGE_URL .'">[Premium]</a>'),
       '#open' => TRUE,
     );

     $form['miniorange_saml_idp_additional_user_attrs']['markup_idp_user_attr_note'] = array(
         '#markup' => '<hr><div class="mo_saml_highlight_background_note" id="ConstAttrNote"><b>Attribute Name: </b>The attribute key sent in the SAML Assertion.
                            <br/><b>User Profile Attribute Value:</b> The user attribute (machine name) whose value will be sent corresponding to the Attribute key</div>',
     );

     $form['miniorange_saml_idp_additional_user_attrs']['user_profile_attribute_table'] = array(
       '#type' => 'table',
       '#header' => [
         'name' => t('Attribute Name'),
         'value' => t('User Profile Attribute Value'),
         'action' => t('Operation'),
       ],
     );

     $form['miniorange_saml_idp_additional_user_attrs']['user_profile_attribute_table'][0]['name'] = array(
       '#type' => 'textfield',
       '#size' => 20,
       '#attributes' => ['placeholder' => 'Enter Attribute Name',],
       '#disabled' => TRUE,
     );

     $form['miniorange_saml_idp_additional_user_attrs']['user_profile_attribute_table'][0]['value'] = array(
       '#type' => 'select',
       '#options' => Utilities::customUserFields(),
     );

     $form['miniorange_saml_idp_additional_user_attrs']['user_profile_attribute_table'][0]['action'] = array(
       '#type' => 'submit',
       '#value' => 'Remove',
       '#disabled' => TRUE,
     );

    $form['miniorange_saml_idp_additional_user_attrs']['user_profile_attribute_add_button'] = array(
      '#type' => 'submit',
      '#value' => 'Add more',
      '#disabled' => TRUE,
    );

     $form['miniorange_saml_idp_constant_attrs'] = array(
       '#type' => 'details',
       '#open' => TRUE,
       '#title' => 'Constant  Attributes <a href="' . $base_url . MiniorangeSAMLIdpConstants::LICENSE_PAGE_URL .'">[Premium]</a>',
     );

     $form['miniorange_saml_idp_constant_attrs']['markup_idp_constant_attr_note'] = array(
         '#markup' => '<hr><div class="mo_saml_highlight_background_note" id="ConstAttrNote"><b>Attribute Name:</b> The attribute key sent in the SAML Assertion.
                            <br /><b>Attribute Value:</b> The constant value you want to send to the Service Provider corresponding to the key.</div>',
     );


     $form['miniorange_saml_idp_constant_attrs']['miniorange_saml_idp_constant_attrs_table'] = array(
       '#type' => 'table',
       '#header' => [
         'name' => 'Attribute Name',
         'value' => 'Attribute Value',
         'operation' => 'Operation',
       ]
     );

     $form['miniorange_saml_idp_constant_attrs']['miniorange_saml_idp_constant_attrs_table'][0]['name'] = array(
       '#type' => 'textfield',
       '#attributes' => ['placeholder' => 'Enter Attribute Name',],
       '#disabled' => TRUE,
     );

     $form['miniorange_saml_idp_constant_attrs']['miniorange_saml_idp_constant_attrs_table'][0]['value'] = array(
       '#type' => 'textfield',
       '#attributes' => ['placeholder' => 'Enter Attribute Name',],
       '#disabled' => TRUE,
     );

     $form['miniorange_saml_idp_constant_attrs']['miniorange_saml_idp_constant_attrs_table'][0]['operation'] = array(
       '#type' => 'submit',
       '#value' => 'Remove',
       '#disabled' => TRUE,
     );

     $form['miniorange_saml_idp_constant_attrs']['miniorange_saml_idp_constant_add_button'] = array(
       '#type' => 'submit',
       '#value' => 'Add more',
       '#disabled' => TRUE,
       '#suffix' => '<br><br>',
     );

     $form['miniorange_saml_idp_constant_attrs']['miniorange_saml_idp_attr_mapping_form_end'] = array(
         '#markup' => '<br><br>'
     );

     $form['miniorange_saml_idp_form_end'] = array(
       '#markup'  => '</div>',
     );

     Utilities::advertise2fa($form, $form_state);

     return $form;

 }
  public function submitForm(array &$form, FormStateInterface $form_state){
      $form_value = $form_state->getValues();
      $nameid_attr = $form_value['miniorange_saml_idp_nameid_attr_map'];
      $nameid_attr_value = $nameid_attr == '' ? 'emailAddress' : $nameid_attr;
      \Drupal::configFactory()->getEditable('miniorange_saml_idp.settings')->set('miniorange_saml_idp_nameid_attr_map', $nameid_attr_value)->save();
      \Drupal::messenger()->addMessage(t('Your settings are saved successfully.'));
  }
}
