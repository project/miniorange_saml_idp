<?php

/**
 * @file
 * Contains \Drupal\miniorange_saml\Form\Export.
 */
namespace Drupal\miniorange_saml_idp\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\miniorange_saml_idp\MiniorangeSAMLIdpConstants;
use Drupal\miniorange_saml_idp\Utilities;

class MiniorangeAdvanceSettings extends FormBase
{
    public function getFormId(){
        return 'miniorange_saml_advance_settings';
    }
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $base_url = \Drupal::request()->getSchemeAndHttpHost() . \Drupal::request()->getBaseUrl();

        $form['markup_library'] = array(
            '#attached' => array(
                'library' => array(
                    'miniorange_saml_idp/miniorange_saml_idp.admin',
                    'core/drupal.dialog.ajax',
                )
            ),
        );
        $idp_entity_id = $base_url . '/?q=admin/config/people/miniorange_saml_idp/';
        $idp_login_url = $base_url . '/initiatelogon';
        \Drupal::configFactory()->getEditable('miniorange_saml_idp.settings')->set('miniorange_saml_idp_entity_id_issuer', $idp_entity_id)->save();
        \Drupal::configFactory()->getEditable('miniorange_saml_idp.settings')->set('miniorange_saml_idp_login_url', $idp_login_url)->save();

        $form['markup_start'] = array(
            '#markup' => t('<div class="mo_saml_table_layout_1"><div class="mo_saml_table_layout mo_saml_container">
                            <div class="mo_saml_idp_font_for_heading">Advance Settings</div><p style="clear: both"></p><hr/>')
        );

        /**
         * Create container to hold @moSAMLAdvanseSettings form elements.
         */
        $form['mo_saml_idp_initiate_sso'] = array(
            '#type' => 'details',
            '#title' => t('IdP Initiated Login' ),
            '#open' => TRUE,
        );

        $form['mo_saml_idp_initiate_sso']['Checkbox'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable Identity Provider Initiated SSO &nbsp;&nbsp;&nbsp;&nbsp;') .'<a href="'. $base_url .'/admin/config/people/miniorange_saml_idp/Licensing">[PREMIUM]</a>',
            '#disabled' => TRUE,
        );

        $form['mo_saml_idp_initiate_sso']['mo_saml_idp_debugging_log'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable logging &nbsp;&nbsp;&nbsp;&nbsp;') .'<a href="'. $base_url .'/admin/config/people/miniorange_saml_idp/Licensing">[PREMIUM]</a>',
            '#description' => t('Enabling this checkbox will add loggers under the <a target="_blank" href="'. $base_url .'/admin/reports/dblog">Reports</a> section '),
            '#suffix' => '<br>',
            '#disabled' => TRUE,
        );

        $form['mo_saml_idp_initiate_sso']['mo_saml_idp_logging'] = array(
            '#type' => 'submit',
            '#value' => t('Save Configuration'),
            '#disabled' => TRUE,
            '#attributes' => array( 'class' => array('button button--primary')),
        );

        /**
         * Create container to hold @moSAMLAdvanseSettings form elements.
         */
        $form['mo_saml_import_export_configurations'] = array(
            '#type' => 'details',
            '#title' => t('Import/Export configurations' ),
            '#open' => TRUE,
        );

        $form['mo_saml_import_export_configurations']['markup_top_2'] = array (
            '#markup' => t('<br><br><div class="mo_saml_font_for_sub_heading">Export Configuration</div><hr/>
                    <div id="Exort_Configuration"><p>Click on the button below to download module configuration.</p>')
        );

        $entity_id = \Drupal::config('miniorange_saml_idp.settings')->get('miniorange_saml_idp_entity_id');
        $ACS_URL = \Drupal::config('miniorange_saml_idp.settings')->get('miniorange_saml_idp_acs_url');
        $disableButton = FALSE;
        if($entity_id == NULL || $ACS_URL == NULL){
            $disableButton = TRUE;
            $form['mo_saml_import_export_configurations']['markup_note'] = array(
                '#markup' => t('<div class="mo_saml_configure_message"> Please <a href="' . $base_url . '/admin/config/people/miniorange_saml_idp/idp_setup">Configure module </a> first to export the configurations.</div><br>'),
            );
        }

        $form['mo_saml_import_export_configurations']['miniorange_saml_imo_option_exists_export'] = array(
            '#type' => 'submit',
            '#value' => t('Download Configuration'),
            '#button_type' => 'primary',
            '#submit' => array('::miniorange_import_export'),
            '#disabled' => $disableButton,
            '#suffix' => '<br/><br/></div><div id="Import_Configuration"><br/>'
        );

        $form['mo_saml_import_export_configurations']['markup_prem_plan'] = array(
            '#markup' => t('<br><div id="import_configurations" class="mo_saml_font_for_sub_heading">Import Configuration</div><hr><br><div class="mo_saml_highlight_background_note_export"><b>Note: </b>Available in
            <a href="' . $base_url . MiniorangeSAMLIdpConstants::LICENSE_PAGE_URL .'">Premium</a> version of the module</div>'),
        );

        $form['mo_saml_import_export_configurations']['import_Config_file'] = array(
            '#type' => 'file',
            '#disabled' => TRUE,
            '#prefix' => '<div class="container-inline">',
        );

        $form['mo_saml_import_export_configurations']['miniorange_saml_idp_import'] = array(
            '#type' => 'submit',
            '#value' => t('Upload'),
            '#disabled' => TRUE,
            '#suffix' => '</div></div></div>',
        );

        Utilities::advertise2fa($form, $form_state);

        return $form;
    }
    function miniorange_import_export()
    {
        $tab_class_name = Utilities::getClassNameForImport_Export();
        $configuration_array = array();
        foreach($tab_class_name as $key => $value) {
            $configuration_array[$key] = self::mo_get_configuration_array($value);
        }
        $configuration_array["Version_dependencies"] = self::mo_get_version_informations();
        header("Content-Disposition: attachment; filename = miniorange_saml_idp_config.json");
        echo(json_encode($configuration_array, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        exit;
    }

    function mo_get_configuration_array($class_name) {
        $class_object = Utilities::getVariableArrayForImport_Export($class_name);
        $mo_array = array();
        foreach($class_object as $key => $value) {
            $mo_option_exists = \Drupal::config('miniorange_saml_idp.settings')->get($value);
            if($mo_option_exists) {
                $mo_array[$key] = $mo_option_exists;
            }
        }
        return $mo_array;
    }

    function mo_get_version_informations() {
        $array_version = array();
        $array_version["PHP_version"] = phpversion();
        $array_version["Drupal_version"] = Utilities::mo_get_drupal_core_version();
        $array_version["OPEN_SSL"] = self::mo_saml_is_openssl_installed();
        $array_version["CURL"] = self::mo_saml_is_curl_installed();
        $array_version["ICONV"] = self::mo_saml_is_iconv_installed();
        $array_version["DOM"] = self::mo_saml_is_dom_installed();
        return $array_version;
    }

    function mo_saml_is_openssl_installed() {
        if (in_array( 'openssl', get_loaded_extensions())) {
            return 1;
        }else {
            return 0;
        }
    }

    function mo_saml_is_curl_installed() {
        if (in_array( 'curl', get_loaded_extensions())) {
            return 1;
        }else {
            return 0;
        }
    }

    function mo_saml_is_iconv_installed() {
        if (in_array( 'iconv', get_loaded_extensions())) {
            return 1;
        } else {
            return 0;
        }
    }

    function mo_saml_is_dom_installed() {
        if (in_array( 'dom', get_loaded_extensions())) {
            return 1;
        } else {
            return 0;
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {

    }
}
