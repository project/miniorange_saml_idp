<?php

/**
 * @file
 * Contains \Drupal\miniorange_saml\Form\MiniorangeSamlCustomerSetup.
 */

namespace Drupal\miniorange_saml_idp\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\miniorange_saml_idp\MiniorangeSAMLIdpConstants;
use Drupal\miniorange_saml_idp\Utilities;

class MiniorangeSamlCustomerSetup extends FormBase
{

    public function getFormId()
    {
        return 'miniorange_saml_customer_setup';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['#attached']['library'][] = 'miniorange_saml_idp/miniorange_saml_idp.admin';
        $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

        $current_status = \Drupal::config('miniorange_saml_idp.settings')->get('miniorange_saml_status');

        if (empty($current_status) || $current_status == 'CUSTOMER_SETUP') {
          $form['markup_top'] = array(
            '#markup' => '<div class="mo_saml_table_layout_1"><div id="Register_Section" class="mo_saml_table_layout mo_saml_container">'
          );

          $form['mo_saml_login'] = array(
            '#type' => 'fieldset',
            '#attributes' => array( 'style' => 'padding:2% 2% 20%; margin-bottom:2%' ),
          );

          $form['mo_saml_login']['markup_15'] = array(
            '#markup' => t('<div class="mo_saml_idp_font_for_heading">Login with mini<span class="mo_orange"><b>O</b></span>range</div><p style="clear: both"></p><hr>'),
          );

          $form['mo_saml_login']['username'] = array(
            '#type' => 'email',
            '#title' => $this->t('Email'),
            '#required' => TRUE,
          );

          $form['mo_saml_login']['password'] = array(
            '#type' => 'password',
            '#title' => $this->t('Password'),
            '#required' => TRUE,
          );

          $form['mo_saml_login']['miniorange_saml_customer_login_button'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Login'),
            '#button_type' => 'primary',
            '#prefix' => '<div class="container-inline">',
          );

          $form['mo_saml_login']['register_link'] = array(
            '#type' => 'link',
            '#title' => $this->t('Create an account'),
            '#url' => Url::fromUri(MiniorangeSAMLIdpConstants::CREATE_ACCOUNT_URL),
            '#attributes' => array('class' => 'button', 'target' => '_blank'),
            '#suffix' => '</div>',
          );

        } elseif ($current_status == 'PLUGIN_CONFIGURATION') {
          $form['markup_top_message'] = array(
            '#markup' => t('<div class="mo_saml_table_layout_1"><div class="mo_saml_table_layout mo_saml_container">')
          );

          /* Create container to hold @UserProfile form elements. */

          $form['mo_saml_user_profile'] = array(
            '#type' => 'fieldset',
            '#attributes' => array( 'style' => 'padding:2% 2% 5%; margin-bottom:2%' ),
          );

          $form['mo_saml_user_profile']['markup_top_'] = array(
            '#markup' => t('<div class="mo_saml_welcome_message">Thank you for registering with miniOrange</div><br><h4>Your Profile: </h4>')
          );

          $header = array( t('Attribute'),  t('Value'),);
          $options = array(
            array( 'Customer Email', \Drupal::config('miniorange_saml_idp.settings')->get('miniorange_saml_idp_customer_admin_email'),),
            array( 'Customer ID', \Drupal::config('miniorange_saml_idp.settings')->get('miniorange_saml_customer_id'),),
            array( 'Drupal Version', Utilities::mo_get_drupal_core_version(),),
            array( 'PHP Version', phpversion(),),
          );

          $form['mo_saml_user_profile']['fieldset']['customerinfo'] = array(
            '#theme' => 'table',
            '#header' => $header,
            '#rows' => $options,
            '#suffix' => '<br>',
            '#attributes' => array( 'style' => 'margin:1% 0% 7%;' ),
          );

          $form['mo_saml_user_profile']['miniorange_saml_customer_Remove_Account_info'] = array(
            '#markup' => t('<h4>Remove Account:</h4><p>This section will help you to remove your current
                       logged in account without losing your current configurations.</p>')
          );

          $form['mo_saml_user_profile']['miniorange_saml_customer_Remove_Account'] = array(
            '#type' => 'link',
            '#title' => $this->t('Remove Account'),
            '#url' => Url::fromRoute('miniorange_saml_idp.modal_form'),
            '#attributes' => [
              'class' => [
                'use-ajax',
                'button',
                ],
            ],
          );
        }

        $form['div_for_end'] = array(
            '#markup' => '</div>'
        );

        Utilities::advertise2fa($form, $form_state);

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
      $username = trim($form_state->getValue('username'));
      $password = $form_state->getValue('password');
      Utilities::customer_setup_submit($username, '', $password, TRUE);
    }
}
