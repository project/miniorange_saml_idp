<?php

namespace Drupal\miniorange_saml_idp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\miniorange_saml_idp\Utilities;

class MiniorangeGenerateCertificate extends FormBase {

  public function getFormId() {
    return 'miniorange_generate_certificate';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'] = array('miniorange_saml_idp/miniorange_saml_idp.admin');

    $form['miniorange_saml_idp_add_certificate'] = array(
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Add Custom Certificate'),
    );

    $form['miniorange_saml_idp_add_certificate']['miniorange_saml_idp_private_certificate'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('X.509 Private Certificate'),
      '#disabled' => TRUE,
      '#attributes' => array('placeholder' => '-----BEGIN CERTIFICATE-----'.PHP_EOL.'XXXXXXXXXXXXXXXXXXXXXXXXXXX'.PHP_EOL.'-----END CERTIFICATE-----'),
      '#description' => t('Copy and paste your private key of X.509 Certificate here.'),
    );

    $form['miniorange_saml_idp_add_certificate']['miniorange_saml_idp_publ_certificate'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('X.509 Public Certificate '),
      '#disabled' => TRUE,
      '#attributes' => array('placeholder' => '-----BEGIN CERTIFICATE-----'.PHP_EOL.'XXXXXXXXXXXXXXXXXXXXXXXXXXX'.PHP_EOL.'-----END CERTIFICATE-----'),
      '#description' => t('Copy and paste your public key of X.509 Certificate here.'),
    );

    $form['miniorange_saml_idp_add_certificate']['save_config_elements'] = array(
      '#type' => 'submit',
      '#name'=>'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Upload'),
      '#submit' => array('::submitForm'),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_add_certificate']['save_config_elements1'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_generate_certificate'] = array(
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Generate Custom Certificate'),
    );

    $form['miniorange_saml_idp_generate_certificate']['mo_saml_country_code_text'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Country Code'),
      '#description' => $this->t('<b>Note: </b>Check your country code <a>here</a>.'),
      '#attributes' => array(
        'placeholder' => $this->t('Enter Country code')
      ),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_generate_certificate']['mo_saml_certificate_state_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('State'),
      '#attributes' => array(
        'placeholder' => $this->t('State Name')
      ),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_generate_certificate']['mo_saml_certificate_company_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Company'),
      '#attributes' => array(
        'placeholder' => $this->t('Company Name')
      ),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_generate_certificate']['miniorange_saml_idp_unit_name'] = array(
      '#type' => 'textfield',
      '#title' => 'Unit',
      '#attributes' => array(
        'placeholder' => $this->t('Unit name')
      ),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_generate_certificate']['mo_saml_certificate_common_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Common'),
      '#attributes' => array(
        'placeholder' => $this->t('Common Name')
      ),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_generate_certificate']['mo_saml_select_digest_algo'] = array(
      '#type' => 'select',
      '#title' => $this->t('Digest Algorithm'),
      '#options' => array(
        'sha512' => $this->t('SHA512'),
        'sha384' => $this->t('SHA384'),
        'sha256' => $this->t('SHA256'),
        'sha1' => $this->t('SHA1'),
      ),
    );

    $form['miniorange_saml_idp_generate_certificate']['mo_saml_select_private_key_bit'] = array(
      '#type' => 'select',
      '#title' => $this->t('Bits to generate the private key'),
      '#options' => array(
        '2048' => $this->t('2048 bits'),
        '1024' => $this->t('1024 bits'),
      ),
    );

    $form['miniorange_saml_idp_generate_certificate']['mo_saml_select_valid_days'] = array(
      '#type' => 'select',
      '#title' => $this->t('Valid Days'),
      '#options' => array(
        '365' => $this->t('365 days'),
        '180' => $this->t('180 days'),
        '90' => $this->t('90 days'),
        '45' => $this->t('45 days'),
        '30' => $this->t('30 days'),
        '15' => $this->t('15 days'),
        '7' => $this->t('7 days'),
      ),
    );

    $form['miniorange_saml_idp_generate_certificate']['generate_config_elements'] = array(
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Generate Self-Signed Certs'),
      '#disabled' => TRUE,
    );

    $form['miniorange_saml_idp_generate_certificate']['clear_genrate_certificate_data'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Clear Data'),
      '#disabled' => TRUE,
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }

}
